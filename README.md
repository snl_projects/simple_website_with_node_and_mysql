basic_website_with_mysql

From to start projects need:

1) Install npm packages:  
  **$> npm install**  
2) Create MySQL Database, open database console and create DB with name mvc_mysql_app:  
  **$> CREATE DATABASE <db_name>;**  
3) Install globally sequelize-cli:  
  **$> npm install -g sequelize-cli**  
4) Make mirgate with sequelize:  
  **4> sequelize db:migrate**