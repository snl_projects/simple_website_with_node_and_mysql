var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const Swig = require('swig')

// Controllers init
const index = require('./controllers/index')
const bands = require('./controllers/band')
const users = require('./controllers/user')

// Controllers init END

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
const swig = new Swig.Swig()
app.engine('html', swig.renderFile)
app.set('view engine', 'html');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// App Routes
app.get('/', index.show)
// Defining route to list and post
app.get('/bands', bands.list)
// Get band by ID
app.get('/band/:id', bands.byId)
// Create band
app.post('/bands', bands.create)
// Update
app.put('/band/:id', bands.update)
// Delete by id
app.delete('/band/:id', bands.delete)
// Defining route to list and post users
app.get('/users', users.list)
app.post('/users', users.create)

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
